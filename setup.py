'''
Este arquivo serve para instalar o app delivery nas configuracoes do
Python3 (diretorios de busca de bibliotecas).

Significa adicionar o diretorio de nosso aplicativo nos diretorios de 
busca de modulos do Python3

Para testar, digite na linha de comando:
------------------------------------------
import sys
sys.path

    Listará todos os diretorios que fazem parte do "PATH" do Python3

========   ATENCAO   ========
Executar na linha de comando: (atencao ao ponto final)
pip install -e .["dev"]

OBS.:
O parametro -e quer dizer para atualizar as dependencias sempre que estas
mudarem no projeto

'''

from setuptools import setup, find_packages


def read(filename):
    return [req.strip() for req in open(filename).readlines()]

setup(
    name='delivery',
    version='0.1.0', # major, minor, patch (fix)
    description='App em Python e Flask para delivery de alimentos',
    packages=find_packages(),
    include_package_data=True,
    install_requires=read('requirements.txt'),
    extras_require={
        'dev': read('requirements-dev.txt')
        },
)
