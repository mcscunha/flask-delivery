# -*- encoding: utf-8 -*-

#
# Este arquivo serve para criar comandos no flask quando executado no bash
#
import click
from tabulate import tabulate
from delivery.ext.db import db      # este "db" é o SQLAlchemy
from delivery.ext.db import models  # noqa

def init_app(app):

    @app.cli.command()
    def create_db():
        '''
        Cria o arquivo db do ZERO - CUIDADO ao usar!'''
        try:
            db.create_all() # este objeto "db" se refere ao SQLAlchemy
            conf_db = app.config['SQLALCHEMY_DATABASE_URI']
            inicio_nome = conf_db.find('///')
            nome_banco = conf_db[inicio_nome+3:]
            click.echo('Banco dados criado com sucesso: ' + nome_banco)
        except:
            print('Erro ao criar o banco de dados. Revise o models')
    
    @app.cli.command()
    @click.option('--email', '-e')  # ao invés de usar decorator, poderia usar o sys.args()
    @click.option('--passwd', '-p')
    @click.option('--admin', '-a', is_flag=True, default=False)
    def add_user(email, passwd, admin):
        '''Adiciona novo usuário'''
        user = models.User(
            email=email,
            passwd=passwd,
            admin=admin
        )
        db.session.add(user)
        db.session.commit()
        print('Usuário criado com sucesso:', email)
    
    @app.cli.command()
    def listar_pedidos():
        '''Lista todos os pedidos feitos'''
        print(tabulate( ('Lista de pedidos',) )) #, headers='firstrow'))
        #click.echo("Lista de pedidos")

    @app.cli.command()
    def listar_usuarios():
        '''Lista todos os usuarios cadastrados'''
        click.echo("Lista de usuarios")
