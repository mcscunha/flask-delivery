def init_app(app):
    app.config['SECRET_KEY'] = '66af88df-9d69-4ec5-b130-48982a1a7b39'
    
    # Definindo o nome do arquivo de banco de dados. 
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///codefoods.db'

    # Configurando o flask para nao exibir msg de alerta no bash em modo debug
    if app.debug:
        app.config['DEBUG_TB_TEMPLATE_EDITOR_ENABLED'] = True
        app.config['DEBUG_TB_PROFILER_ENABLED'] = True
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True