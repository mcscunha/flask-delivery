env:
	cd /cygdrive/c/ProgramData/Anaconda3/Scripts; ./activate base

clean:
	rm -rf .cache
	rm -rf build
	rm -rf *egg-info

install:
	pip install -e .['dev']

uninstall:
	pip uninstall delivery