'''
    Arquivo de configuracao da biblioteca pytest deste projeto
    Aqui colocamos todos os modulos que serao carregados apenas uma vez para
os teste unitarios colocados nos arquivos "test_*.py"
    Se quiser carregar uma funcao ou um objeto ou uma classe e esta ficar
disponivel para todos os testes unitarios, este é o local para definir.
    O nome dos arquivos de testes deve ter, obrigatoriamente, o prefixo 
"test_" - É uma convencao, mas é bom manter o padrao

Para rodar os testes, execute na linha de comando:
# Com pytest e pytest-flask
pytest ./tests -v

# Com pytest-cov, use assim:
pytest ./tests -v --cov=delivery

'''

import pytest
from delivery.app import create_app


@pytest.fixture(scope='module')
def app():
    '''Instancia principal do App Flask'''
    return create_app()